![gopher-copper](/var/gopher-copper.jpg)

# Gopper

## Synopsis

`gopper -addr=address[,address,...] [-polling=time] [-syscalls=name,...] pid`

## Description

Gopper is a Linux tool designed to watch a process and detect suspicious
behaviors while it's analyzed with other tools. It doesn't interfere with
the watched process. This program can be used with other reversing and analysis
tools, in order to detect evasion techniques and other malicious conducts.

This program must be run with root privileges.

Gopper watches the memory addresses passed as arguments to detect:

* Modifications of the permissions of the corresponding pages. It also
warns about dangerous permissions (i.e. write+exec). To do that, it polls
`/proc/pid/maps`.

* Calls to the `mprotect` syscall, used to change page permissions. This is
done by receiving events from the *Linux kernel tracepoints* through the
synthetic files located in `/sys/kernel/debug/tracing`.
See https://www.kernel.org/doc/Documentation/trace/events.txt
for more info.

* Calls to other syscalls, defined by the user.

* Modifications of the corresponding memory pages. This is done by polling
the pages' *soft dirty bit*, available through `/proc/pid/pagemap`.
It can be used to detect code/data modifications in malicious programs.  

* Operations over `/proc/pid/clear_refs`. This synthetic file is used to clear
the *soft dirty bit* of the process' pages. The malicious program
could use this file to hide some page changes (by changing a page and
clear the soft dirty bit while the polling is done, i.e. between two reads).
This is done by using the *inotify(7)* mechanism to detect accesses to
`/proc/PID/clear_refs`. Gopper uses the `k8s.io/utils/inotify` Go package
to receive inotify events.

Mandatory arguments:

* `pid`  process id

* `-addr=`  at least one memory address must be provided

Optional arguments:

* `-polling=` defines the polling time in milliseconds. By default, it's 400 ms.  

* `-syscalls=` adds extra syscalls to be monitored.

## Example

In the following example, PID  24841 runs a malicious binary being analyzed by
Frida-trace. The program modifies the Frida preludes in order to bypass the
interception mechanisms (see https://sysfatal.github.io/bypassfrida-en.html
for more info about this evasion technique):

```
$ ./gopper -addr=7f777e5c61d0,7fffe69c6757 -syscalls=clone,read 24841
2021/12/15 20:13:30 watching pid: 24841
2021/12/15 20:13:30 watching address: 7f777e5c61d0
2021/12/15 20:13:30 watching address: 7fffe69c6757
2021/12/15 20:13:30 critical: 7f777e5c61d0 page writable and executable
2021/12/15 20:13:34 warning: mprotect detected for 7fffe69c6757
2021/12/15 20:13:34 critical: mprotect w+x detected for 7fffe69c6757
2021/12/15 20:13:34 warning: syscall read detected:         readfile-24841   [001] .... 15133.617503: sys_read(fd: 4, buf: 7fffe69c6330, count: 400)
2021/12/15 20:13:34 warning: syscall read detected:         readfile-24841   [001] .... 15133.617559: sys_read(fd: 4, buf: 7fffe69c6330, count: 400)
2021/12/15 20:13:34 warning: syscall read detected:         readfile-24841   [001] .... 15133.618244: sys_read(fd: 0, buf: 7fffe69c6787, count: 1)
2021/12/15 20:13:34 critical: page of address 7f777e5c61d0 was modified
2021/12/15 20:13:34 critical: page of address 7fffe69c6757 was modified
2021/12/15 20:14:26 critical: soft dirty could be cleared "/proc/24841/clear_refs": 0x2 == in_modify
2021/12/15 20:14:26 critical: soft dirty could be cleared "/proc/24841/clear_refs": 0x20 == in_open
2021/12/15 20:14:26 critical: soft dirty could be cleared "/proc/24841/clear_refs": 0x2 == in_modify
2021/12/15 20:14:26 critical: soft dirty could be cleared "/proc/24841/clear_refs": 0x8 == in_close_write
2021/12/15 20:14:32 process (24841) died
$
```

## Author

Written by Enrique Soriano \<esoriano@gsyc.urjc.es\>

## License

MIT license

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
