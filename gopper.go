/*
Copyright (C) 2021 Enrique Soriano

MIT license

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

package main

import (
	"bufio"
	"encoding/binary"
	"errors"
	"flag"
	"fmt"
	"k8s.io/utils/inotify"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

const (
	R = iota
	W
	X
	DefPollTime  = 400 // ms
	PageSize     = 4 * 1024
	SoftDirtyBit = 55
	ProtWX       = 6 // from sys/mman.h PROT_WRITE=0x2,PROT_EXEC=0x4
)

var (
	pid = -1 // set by main, never modified
)

type Watchpoint struct {
	sync.Mutex
	init     bool
	addr     uint64
	wasdirty bool   // page's soft dirty bit
	perms    []bool // page's permissions: r,w,x
}

func NewWatchPoint(a string) *Watchpoint {
	addr, err := strconv.ParseUint(a, 16, 64)
	if err != nil {
		log.Fatal("bad address", a, err)
	}
	r := &Watchpoint{}
	r.addr = addr
	r.perms = []bool{false, false, false}
	return r
}

// Watchpoint methods for polling
// Caller must adquire the mutex before calling these methods
func (w *Watchpoint) IsDirty() bool {
	path := fmt.Sprintf("/proc/%d/pagemap", pid)
	f, err := os.OpenFile(path, os.O_RDONLY, os.FileMode(0))
	if err != nil {
		sysFatal("can't open clear_refs")
	}
	offset := int64(w.addr / PageSize * 8)
	buf := make([]byte, 8)
	_, err = f.ReadAt(buf, offset)
	if err != nil { //it should not fail (or short read)
		sysFatal("can't read from pagemap")
	}
	f.Close()
	x := binary.LittleEndian.Uint64(buf)
	return (x>>SoftDirtyBit)&1 == 1
}

func (w *Watchpoint) CheckPerms(p [3]bool) {
	changed := false
	strs := []string{"readable", "writable", "executable"}
	for i, _ := range p {
		if w.perms[i] != p[i] {
			changed = true
			w.perms[i] = p[i]
			if w.init {
				if p[i] {
					log.Printf("warning: %x page " +
						"now is %s\n", w.addr, strs[i])
				} else {
					log.Printf("warning: %x page now " +
						"is not %s\n", w.addr, strs[i])
				}
			}
		}
	}
	if (changed || !w.init) && p[X] && p[W] {
		log.Printf("critical: %x page writable and executable", w.addr)
	}
	w.init = true
}

func resetDirty() {
	path := fmt.Sprintf("/proc/%d/clear_refs", pid)
	f, err := os.OpenFile(path, os.O_WRONLY, os.FileMode(0))
	if err != nil {
		sysFatal("can't open clear_refs")
	}
	b := []byte{'4'}
	_, err = f.Write(b)
	if err != nil {
		sysFatal("can't write clear_refs")
	}
	f.Close()
}

// watches accesses to the /proc file used to clear the soft dirty bit
func refWatcher() {
	watcher, err := inotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	path := fmt.Sprintf("/proc/%d/clear_refs", pid)
	err = watcher.Watch(path)
	if err != nil {
		sysFatal(err)
	}
	for {
		select {
		case ev := <-watcher.Event:
			log.Println("critical: soft dirty could be cleared", ev)
		case err := <-watcher.Error:
			log.Println("inotify error:", err)
		}
	}
}

func dirtyPolling(points []*Watchpoint, t int) {
	for {
		time.Sleep(time.Duration(t) * time.Millisecond)
		for _, val := range points {
			val.Lock()
			if val.IsDirty() {
				if !val.wasdirty {
					val.wasdirty = true
					log.Printf("critical: page of " +
						"address %x was modified",
						val.addr)
				}
			} else {
				val.wasdirty = false
			}
			val.Unlock()
		}
	}
}

func getAddrs(line string) (from uint64, to uint64, err error) {
	s1 := strings.Split(line, "-")
	if len(s1) < 2 {
		return 0, 0, errors.New("bad line")
	}
	s2 := strings.Split(s1[1], " ")
	if len(s2) < 2 {
		return 0, 0, errors.New("bad line")
	}
	from, err = strconv.ParseUint(s1[0], 16, 64)
	if err != nil {
		return 0, 0, errors.New("can't convert from")
	}
	to, err = strconv.ParseUint(s2[0], 16, 64)
	if err != nil {
		return 0, 0, errors.New("can't convert to")
	}
	return
}

func getPerms(line string) (p [3]bool, err error) {
	s1 := strings.Split(line, " ")
	if len(s1) < 2 {
		return p, errors.New("bad line")
	}
	p[R] = []rune(s1[1])[0] == 'r'
	p[W] = []rune(s1[1])[1] == 'w'
	p[X] = []rune(s1[1])[2] == 'x'
	return
}

func mapsPolling(points []*Watchpoint, t int) {
	for {
		time.Sleep(time.Duration(t) * time.Millisecond)
		path := fmt.Sprintf("/proc/%d/maps", pid)
		f, err := os.OpenFile(path, os.O_RDONLY, os.FileMode(0))
		if err != nil {
			sysFatal("can't open maps")
		}
		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			from, to, err := getAddrs(scanner.Text())
			if err != nil {
				log.Print("error parsing maps addrs: " +
					scanner.Text())
				continue
			}
			p, err2 := getPerms(scanner.Text())
			if err2 != nil {
				log.Print("error parsing maps perms: " +
					scanner.Text())
				continue
			}
			for _, val := range points {
				val.Lock()
				if val.addr >= from && val.addr < to {
					val.CheckPerms(p)
				}
				val.Unlock()
			}
		}
		if err := scanner.Err(); err != nil {
			sysFatal("can't read maps")
		}
		f.Close()
	}
}

func setEvent(syscalls []string) {
	path := "/sys/kernel/debug/tracing/set_event"
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_APPEND, os.FileMode(0))
	if err != nil {
		sysFatal("can't open set_event")
	}
	if syscalls == nil {
		s := "\n"
		_, err = f.Write([]byte{})
		if err != nil {
			log.Fatal("can't write set_event: " + s)
		}
	} else {
		for _, val := range syscalls {
			s := "syscalls:sys_enter_" + val
			_, err = f.Write([]byte(s))
			if err != nil {
				log.Fatal("can't write set_event: " + s)
			}
		}
	}
	f.Close()
}

func setCurrentTracer() {
	path := "/sys/kernel/debug/tracing/current_tracer"
	f, err := os.OpenFile(path, os.O_WRONLY, os.FileMode(0))
	if err != nil {
		sysFatal("can't open current_tracer")
	}
	_, err = f.Write([]byte("nop"))
	if err != nil {
		log.Fatal("can't write current_tracer")
	}
	f.Close()
}

func setEventpid() {
	path := "/sys/kernel/debug/tracing/set_event_pid"
	f, err := os.OpenFile(path, os.O_WRONLY, os.FileMode(0))
	if err != nil {
		log.Fatal("can't set_event_pid")
	}
	s := fmt.Sprintf("%d", pid)
	_, err = f.Write([]byte(s))
	if err != nil {
		log.Fatal("can't write set_event_pid")
	}
	f.Close()
}

func enableTracing(on bool) {
	path := "/sys/kernel/debug/tracing/tracing_on"
	f, err := os.OpenFile(path, os.O_WRONLY, os.FileMode(0))
	if err != nil {
		log.Fatal("can't tracing_on")
	}
	x := 0
	if on {
		x = 1
	}
	s := fmt.Sprintf("%d", x)
	_, err = f.Write([]byte(s))
	if err != nil {
		log.Fatal("can't write tracing_on")
	}
	f.Close()
}

func mprotectArgs(l string) (uint64, uint64, uint64) {
	a := strings.Split(l, "start: ")
	b := strings.Split(a[1], ", len: ")
	c := strings.Split(b[1], ", prot: ")
	d := strings.Split(c[1], ")")
	start, err1 := strconv.ParseUint(b[0], 16, 64)
	len, err2 := strconv.ParseUint(c[0], 16, 64)
	prot, err3 := strconv.ParseUint(d[0], 16, 32)
	if err1 != nil || err2 != nil || err3 != nil {
		log.Fatal("error parsing mprotect args")
	}
	return start, len, prot
}

func checkCall(points []*Watchpoint, l string) {
	start, len, prot := mprotectArgs(l)
	for _, val := range points {
		val.Lock()
		if val.addr >= start && val.addr < start+len {
			log.Printf("warning: mprotect detected " +
				"for %x", val.addr)
			if prot == ProtWX {
				log.Printf("critical: mprotect w+x " +
					"detected for %x", val.addr)
			}
		}
		val.Unlock()
	}
}

func otherCall(calls []string, l string) {
	for _, val := range calls {
		if strings.Contains(l, "sys_"+val+"(") {
			log.Printf("warning: syscall %s detected: %s", val, l)
			break
		}
	}
}

func syscallWatcher(points []*Watchpoint, syscalls string) {
	path := "/sys/kernel/debug/tracing/trace_pipe"
	extracalls := strings.Split(syscalls, ",")
	calls := append(extracalls, "mprotect")
	enableTracing(false)
	setEvent(nil)
	setEvent(calls)
	setCurrentTracer()
	setEventpid()
	enableTracing(true)
	f, err := os.OpenFile(path, os.O_RDONLY, os.FileMode(0))
	if err != nil {
		sysFatal("can't open trace_pipe")
	}
	scanner := bufio.NewScanner(f)
	s := "sys_mprotect\\(.*, prot: [0-9a-f]+\\)$"
	re, err2 := regexp.Compile(s)
	if err2 != nil {
		log.Fatal("bug, bad regexp")
	}
	for scanner.Scan() {
		if re.MatchString(scanner.Text()) {
			checkCall(points, scanner.Text())
		} else { //not a mprotect
			otherCall(calls, scanner.Text())
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal("can't read trace_pipe")
	}
	f.Close()
}

func isProcAlive() bool {
	p, err := os.FindProcess(pid)
	if err != nil {
		return false
	}
	err = p.Signal(syscall.Signal(0))
	return err == nil
}

func sysFatal(a interface{}) {
	if !isProcAlive() {
		log.Fatal(fmt.Sprintf("process (%d) died", pid))
	}
	log.Fatal(a)
}

func usage() {
	fmt.Fprint(os.Stderr,
		"usage: %s -addr=address,address,... [-polling=ms]pid\n",
		os.Args[0])
	os.Exit(1)
}

func main() {
	var err error
	pollingt := DefPollTime
	addrs := flag.String("addr", "noaddress", "-addr=address,address...")
	polls := flag.String("polling", "nopolling", "-polling=time in ms")
	syscalls := flag.String("syscalls", "", "-polling=name,name..")

	flag.Parse()
	if addrs == nil || len(flag.Args()) != 1 {
		usage()
	}
	if *polls != "nopolling" {
		pollingt, err = strconv.Atoi(*polls)
		if err != nil || pollingt <= 0 {
			usage()
		}
	}
	pid, err = strconv.Atoi(flag.Args()[0])
	if err != nil || pid <= 0 {
		usage()
	}
	if !isProcAlive() {
		fmt.Fprintf(os.Stderr, "process (%d) does not exist\n", pid)
		os.Exit(1)
	}
	a := strings.Split(*addrs, ",")
	log.Printf("watching pid: %d\n", pid)
	points := make([]*Watchpoint, len(a))
	for i, val := range a {
		points[i] = NewWatchPoint(val)
		log.Printf("watching address: %x", points[i].addr)
	}
	resetDirty()
	go refWatcher()
	go dirtyPolling(points, pollingt)
	go syscallWatcher(points, *syscalls)
	mapsPolling(points, pollingt)
}
